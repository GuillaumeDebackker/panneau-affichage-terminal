package fr.montras.team;

import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Team {

	private String name;
	private Color color;
	private int score;
	private int fool;
	
	private Rectangle rectangle;
	private Text scoreText;
	private Text foolText;
	
	public Team(String name, Color color, int score, int fool) {
		this.name = name;
		this.color = color;
		this.score = score;
		this.fool = fool;
	}
	
	public void render(Group parent,double x,int y,double width,double height){
		rectangle = new Rectangle(x, y, width, height);
		rectangle.setFill(color);
		
		Text text = new Text(x + width / 2 - 100, 300,name);
		text.setFont(Font.font("Verdana",FontWeight.BOLD, 48));
		text.setFill(Color.WHITE);
		text.setEffect(new DropShadow(6, Color.BLACK));
		
		scoreText = new Text(x + width / 2, height / 2,score + "");
		scoreText.setFont(Font.font("Verdana",FontWeight.BOLD, 48));
		scoreText.setFill(Color.WHITE);
		
		foolText = new Text((x >= width ? width * 2 - 100 : 100), 100, fool + "");
		foolText.setFont(Font.font("Verdana",FontWeight.BOLD, 48));
		foolText.setFill(Color.WHITE);
		
		parent.getChildren().add(rectangle);
		parent.getChildren().add(text);
		parent.getChildren().add(scoreText);
		parent.getChildren().add(foolText);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
		this.rectangle.setFill(color);
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
		this.scoreText.setText(score + "");
	}
	
	public int getFool() {
		return fool;
	}
	
	public void setFool(int fool) {
		this.fool = fool;
		this.foolText.setText(fool + "");
	}
}
