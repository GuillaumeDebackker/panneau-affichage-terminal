package fr.montras.team;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

public class TeamManager {

	private Map<String, Team> teams;
	
	public TeamManager() {
		this.teams = new HashMap<>();
	}
	
	public void init(){
		this.addTeam("Locaux", Color.ORANGE);
		this.addTeam("Visiteur", Color.BLUE);
	}
	
	public void render(Scene scene,Group group){
		int x = 1;
		for(Team team : teams.values()){
			team.render(group, (scene.getWidth() / 2) * x, 0, scene.getWidth() / 2, scene.getHeight());
			x--;
		}
	}
	
	public Team getTeam(String name){
		return teams.get(name);
	}
	
	private void addTeam(String name,Color color){
		this.teams.put(name, new Team(name, color, 0, 0));
	}
}
