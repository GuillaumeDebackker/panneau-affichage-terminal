package fr.montras.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import fr.montras.Display;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class TimerRunnable extends TimerTask{

	
	private Timer timer;
	private int time;
	private Text textTimer;
	
	public TimerRunnable(Group group, int width) {
		this.time = 20 * 60;
		this.textTimer = new Text(width - 75 , 100, getDate(time));
		textTimer.setFont(Font.font("Verdana",FontWeight.BOLD, 48));
		textTimer.setFill(Color.WHITE);
		
		group.getChildren().add(textTimer);
	}
	
	public void start(){
		// TIMER INIT
		this.timer = new Timer();
		this.timer.schedule(this, 0,1000);
	}

	@Override
	public void run() {
		if(!Display.start) return;
		time--;
		textTimer.setText(getDate(time));
	}
	
	public String getDate(int millis){
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		return sdf.format(new Date(millis * 1000));
	}
}
