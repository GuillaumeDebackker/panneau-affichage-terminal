package fr.montras.task;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import fr.montras.Display;
import fr.montras.team.Team;
import fr.montras.team.TeamManager;
import javafx.scene.paint.Color;

public class ConsoleRunnable extends TimerTask{

	
	private Timer timer;
	private TeamManager teamManager;
	
	public ConsoleRunnable(TeamManager teamManager) {
		this.teamManager = teamManager;
	}
	
	public void start(){
		// TIMER INIT
		this.timer = new Timer();
		this.timer.schedule(this, 0,1000);
	}

	@Override
	public void run() {
		Scanner scanner = new Scanner(System.in);
		if(scanner.hasNextLine()){
			String[] args = scanner.nextLine().split(" ");
			if(args.length == 3){
				Team team = teamManager.getTeam(args[1]);
				if(team == null) return;
				
				if(args[0].equalsIgnoreCase("score")){
					team.setScore(Integer.parseInt(args[2]));
					System.out.println("La team " + team.getName() + " ont maintenant " + team.getScore());
				}else if(args[0].equalsIgnoreCase("fool")){
					team.setFool(Integer.parseInt(args[2]));
					System.out.println("La team " + team.getName() + " ont maintenant " + team.getFool() + " fautes");
				}if(args[0].equalsIgnoreCase("color")){
					team.setColor(Color.valueOf(args[2]));
					System.out.println("La team " + team.getName() + " ont maintenant " + team.getColor());
				}
			}else if(args.length == 1){
				if(args[0].equalsIgnoreCase("start")){
					Display.start = true;
				}else if(args[0].equalsIgnoreCase("stop")){
					Display.start = false;
				}
			}
		}
	}
}
