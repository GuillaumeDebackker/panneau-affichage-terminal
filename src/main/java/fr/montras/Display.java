package fr.montras;

import java.awt.Dimension;
import java.awt.Toolkit;

import fr.montras.task.ConsoleRunnable;
import fr.montras.task.TimerRunnable;
import fr.montras.team.TeamManager;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Display extends Application{

	private Group root;
	private Scene scene;
	
	private TeamManager teamManager;
	
	public static boolean start = false;
	
	@Override
	public void start(Stage stage){
		stage.setTitle("DisplayLCD");
		stage.setResizable(false);
		
		
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.root = new Group();
		this.scene = new Scene(root, dimension.getWidth(), dimension.getHeight());
		
		this.teamManager = new TeamManager();
		this.teamManager.init();
		this.teamManager.render(scene,root);
		
		ConsoleRunnable consoleRunnable = new ConsoleRunnable(teamManager);
		consoleRunnable.start();
		
		TimerRunnable timerRunnable = new TimerRunnable(root, (int) (dimension.getWidth() / 2));
		timerRunnable.start();
		
		stage.setScene(scene);
		stage.show();
	}
}
